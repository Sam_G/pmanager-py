# MIT License
#
# Copyright (c) [2023] [S J GEORGE]
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


from Crypto.Cipher import AES
from Crypto.Protocol.KDF import scrypt
from Crypto.Random import get_random_bytes


def generate_key(password: str, salt=None) -> tuple:
    """
    This function generates a 32-byte (256bit) key for use in AES encryption using
    the scrypt KDF. The salt is generated randomly unless explicitly given.

    :param password: String containing the password given by the user
    :param salt: The Salt for the AES Encryption
    :return: A byte-array containing the key
    :return: A byte-array containing the salt
    """

    if salt is None:
        salt = get_random_bytes(32)

    key = scrypt(password, salt, 32, 2 ** 18, r=8, p=4)
    return key, salt


def encrypt_string(plain_text: str, key: bytearray) -> tuple:
    """
    Encrypts the string provided as an input using 256-bit AES in GCM mode

    :param plain_text: A string containing the plain text to be encrypted
    :param key: A byte-array containing the key
    :return: A byte-array containing the cipher-text
    :return: A byte array containing the nonce
    :return: A byte-array containing the tag
    """

    cipher = AES.new(key, AES.MODE_GCM)
    cipher_text = cipher.encrypt(bytes(plain_text, encoding='utf-8'))
    tag = cipher.digest()

    return cipher_text, cipher.nonce, tag


def decrypt_string(cipher_text: bytearray, key: bytearray, nonce: bytearray, tag: bytearray) -> str:
    """
    Decrypts the string provided using 256-bit AES in GCM mode. Calculates a tag for
    the data and compares it to the provided tag - errors if they don't verify

    :param cipher_text: The input cipher text to be decrypted
    :param key: A byte-array containing the key 
    :param nonce: A byte array containing the nonce
    :param tag: A byte-array containing the reference tag
    :return: The decrypted plain text
    """

    cipher = AES.new(key, AES.MODE_GCM, nonce=nonce)
    plain_text = cipher.decrypt(cipher_text)

    try:
        cipher.verify(tag)
    except ValueError as e:
        raise e

    return plain_text
