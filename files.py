# MIT License
#
# Copyright (c) [2023] [S J GEORGE]
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os


def write_file(filename: str, cipher_text: bytearray, nonce: bytearray, tag: bytearray, salt: bytearray):
    """
    Takes the cipher text, tag and salt and writes it to a file in the correct
    format

    :param filename: Name of the file to write to
    :param cipher_text: The encrypted text to be written to the file
    :param nonce: The nonce for the encrypted data
    :param tag: The tag to verify the integrity of the encrypted file
    :param salt: The salt for the key-derivation algorithm
    """

    file_out = open(filename, 'wb')

    file_out.write(salt)
    file_out.write(nonce)
    file_out.write(cipher_text)
    file_out.write(tag)


def read_file(filename: str) -> tuple:
    """
    Gets the cipher text, tag and salt from the provided file

    :param filename: Name of the file to read from
    :return: Cipher text
    :return: nonce
    :return: tag
    :return: salt
    """

    file_in = open(filename, 'rb')
    data_size = os.path.getsize(filename) - 32 - 16 - 16

    salt = file_in.read(32)
    nonce = file_in.read(16)
    cipher_text = file_in.read(data_size)
    tag = file_in.read(16)

    return cipher_text, nonce, tag, salt
