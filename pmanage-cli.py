# MIT License
#
# Copyright (c) [2023] [S J GEORGE]
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from database import PasswordManager
import os
import getpass


def main():
    PATH = os.getenv('HOME') + "/.local/passwords.enc"
    database = PasswordManager()

    if os.path.exists(PATH):
        password = getpass.getpass(prompt="Master Password: ")
        database.read_database(PATH, password)
    else:
        print("Welcome to PManage! Type in a strong master-password to setup your database")
        password = getpass.getpass(prompt="Master Password: ")

    print("a: Add Entry")
    print("g: Get Entry")
    print("l: List Entries")
    print("d: Delete Entry")
    print("q: Save & Quit")

    while True:

        entry = input("> ")

        if entry == "a":
            name = input("Name: ")
            p_word = getpass.getpass(prompt="Password: ")
            database.add_entry(name, p_word)
        elif "g" == entry:
            name = input("Name: ")
            pword = database.get_entry(name)
            if pword is None:
                print("Name not found")
            else:
                print(pword)
        elif entry == "d":
            name = input("Name: ")
            if database.remove_entry(name) is None:
                print("Name not found")
        elif entry == "q":
            database.write_database(PATH, password)
            break
        elif entry == "l":
            for i in range(0, len(database.names)):
                print(database.names[i])
        else:
            print(entry + ": Option not found")


if __name__ == "__main__":
    main()
