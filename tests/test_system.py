"""
MIT License

Copyright (c) [2023] [S J GEORGE]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from crypto import *
from files import *
from database import *


def test_encryption():
    ref_input = "abcdefghijklmnopqrstuvwxyz123456789"
    key, salt = generate_key("password")

    cipher_text, nonce, tag = encrypt_string(ref_input, key)
    plain_text = decrypt_string(cipher_text, key, nonce, tag)

    assert bytes(ref_input, encoding='utf-8') == plain_text


def test_files():
    ref_salt = b'\xf0\xc61+\xf6O\x83I\xc5\xb1y\xd3.\xe7<J\xdd\x8aV\x91b\xa7\x0bD\xd1\xd19$e\xedS\xc7'
    ref_cipher_text = b'K\xe7\xd7\xcc\xffL\xcd\x97\x7f\x1d\xfa\xd9P\x8bR\x96|6\xd5\xbc9\xaa\x7fI\x8c\xa2"B\x81\nU\xb5O\xe2='
    ref_nonce = b'\xfb\x95\x8ez\xb2\xe6=$\xael\xe8M,\xb5\xcdx'
    ref_tag = b"\x9d\xb5'\xcfB8=p\xe5\xa2\x97\xd8\xfd\x88\xf0>"

    filename = "test.enc"

    write_file(filename, ref_cipher_text, ref_nonce, ref_tag, ref_salt)

    cipher_text, nonce, tag, salt = read_file(filename)

    assert cipher_text == ref_cipher_text
    assert nonce == ref_nonce
    assert tag == ref_tag
    assert salt == ref_salt


def test_file_decrypt():
    filename = "test.enc"
    ref_input = "abcdefghijklmnopqrstuvwxyz123456789"
    key, salt = generate_key("password")

    cipher_text, nonce, tag = encrypt_string(ref_input, key)

    write_file(filename, cipher_text, nonce, tag, salt)
    cipher_text, nonce, tag, salt = read_file(filename)

    plain_text = decrypt_string(cipher_text, key, nonce, tag)

    assert bytes(ref_input, encoding='utf-8') == plain_text


def test_database():
    filename = "test.enc"
    database = PasswordManager()

    database.add_entry("name1", "password1")
    database.add_entry("name2", "password2")
    database.add_entry("name3", "password3")

    database.write_database(filename, "password")

    new_database = PasswordManager()
    new_database.read_database(filename, "password")

    assert len(new_database.names) == len(new_database.passwords)
    assert len(new_database.names) == len(database.names)
    assert len(new_database.passwords) == len(database.passwords)

    for i in range(0, len(new_database.passwords)):
        assert new_database.names[i] == database.names[i]
        assert new_database.passwords[i] == database.passwords[i]


def test_database_remove():
    database = PasswordManager()
    print(database.names)

    database.add_entry("name1", "password1")
    database.add_entry("name2", "password2")
    database.add_entry("name3", "password3")

    print(database.names)

    database.remove_entry("name2")

    assert len(database.names) == 2
    assert len(database.passwords) == 2
    assert database.names[0] == "name1"
    assert database.passwords[0] == "password1"
    assert database.names[1] == "name3"
    assert database.passwords[1] == "password3"
