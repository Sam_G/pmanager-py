# MIT License
#
# Copyright (c) [2023] [S J GEORGE]
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import files
import crypto


class PasswordManager:
    """
    The class which represents the password manager database.
    """

    def __init__(self):
        self.passwords = []
        self.names = []

    def add_entry(self, name: str, password: str):
        """
        Adds an entry to the database.

        :param name: The name of the password entry.
        :param password:    The password to be saved.
        """
        self.passwords.append(password)
        self.names.append(name)

    def get_entry(self, name: str) -> str:
        """
        Gets the value of an entry in the database.

        :param name: The name of the password to search.
        :return: The recovered password.
        """
        try:
            index = self.names.index(name)
        except ValueError:
            return None
        return self.passwords[index]

    def remove_entry(self, name: str) -> int:
        """
        Removes a provided password.

        :param name: The name of the password to remove.
        """
        try:
            index = self.names.index(name)
        except ValueError:
            return None

        self.names.pop(index)
        self.passwords.pop(index)
        return 1

    def write_database(self, path: str, password: str):
        """
        Writes the content of the database to the provided file-path

        :param path: The file to write the database to
        :param password: The password to use to generate the encryption key
        """

        output = ""
        for i in range(0, len(self.passwords)):
            output += self.names[i] + '\n'
            output += self.passwords[i] + '\n'

        output = output.rstrip(output[-1])

        key, salt = crypto.generate_key(password)
        cipher_text, nonce, tag = crypto.encrypt_string(output, key)
        files.write_file(path, cipher_text, nonce, tag, salt)

    def read_database(self, path: str, password: str):
        """
        Reads and decrypts the file at the path and populates the database

        :param path: The file to read the database from
        :param password: The password to use to generate the encryption key
        """

        cipher_text, nonce, tag, salt = files.read_file(path)
        key, salt = crypto.generate_key(password, salt)
        plain_text = crypto.decrypt_string(cipher_text, key, nonce, tag).decode("utf-8")

        lines = plain_text.split('\n')
        self.names = lines[0::2]
        self.passwords = lines[1::2]
