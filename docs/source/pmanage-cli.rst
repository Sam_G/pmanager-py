pmanage\-cli
===================

This script provides a simple way to interface with a pmanage password database from the command line. To use, simply run
``python3 pmanage-cli.py``. If the program does not detect a database file (default: ``$HOME/.local/passwords.enc``), a
new database will be created. The user will then enter a menu allowing them to perform a number of options:

- `a`: Add an entry.
- `g`: Get an entry.
- `l`: List the names of all entries.
- `d`: Delete an entry.
- `q`: Save and quit.
